package com.example.demo.service;

import com.example.demo.model.CarModel;
import com.example.demo.service.base.AbstractBaseService;

public interface CarModelService extends AbstractBaseService<CarModel> {

}
