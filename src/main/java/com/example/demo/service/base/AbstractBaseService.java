package com.example.demo.service.base;

import java.util.List;
import java.util.Optional;

public interface AbstractBaseService<T> {
    Optional<T> get(String id);
    List<T> getAll();
    List<T> get(List<String> ids);
    void save(T obj);
}
