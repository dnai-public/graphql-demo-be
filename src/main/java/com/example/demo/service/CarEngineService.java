package com.example.demo.service;

import com.example.demo.api.input.filters.EngineFilter;
import com.example.demo.model.AbstractCarEngine;
import com.example.demo.model.CarModel;
import com.example.demo.service.base.AbstractBaseService;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface CarEngineService extends AbstractBaseService<AbstractCarEngine> {

    default List<AbstractCarEngine> filter(List<AbstractCarEngine> in, EngineFilter filter){
        if(filter == null) return in;

        var minPower = Optional.ofNullable(filter.powerMin()).orElse(Integer.MIN_VALUE);
        var maxPower = Optional.ofNullable(filter.powerMax()).orElse(Integer.MAX_VALUE);
        var minTorque = Optional.ofNullable(filter.torqueMin()).orElse(Integer.MIN_VALUE);
        var maxTorque = Optional.ofNullable(filter.torqueMax()).orElse(Integer.MAX_VALUE);
        var models = new HashSet<>(Optional.ofNullable(filter.models()).orElse(List.of()));


        return in.stream()
                .filter(t -> minPower <= t.getPower())
                .filter(t -> t.getPower() <= maxPower)
                .filter(t -> minTorque <= t.getTorque())
                .filter(t -> t.getTorque() <= maxTorque)
                .filter(t -> {
                    if(models.size() < 1) return true;
                    return t.getModels().stream()
                            .map(CarModel::getModelNumber)
                            .anyMatch(models::contains);
                })
                .collect(Collectors.toList());
    }
}
