package com.example.demo.service;

import com.example.demo.model.CarMaker;
import com.example.demo.service.base.AbstractBaseService;

public interface CarMakerService extends AbstractBaseService<CarMaker> {

}
