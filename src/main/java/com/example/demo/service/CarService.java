package com.example.demo.service;

import com.example.demo.model.Car;
import com.example.demo.service.base.AbstractBaseService;

public interface CarService extends AbstractBaseService<Car> {
}
