package com.example.demo.service.impl;

import com.example.demo.model.AbstractCarEngine;
import com.example.demo.repository.EngineRepository;
import com.example.demo.service.CarEngineService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarEngineServiceImpl implements CarEngineService {
    private final EngineRepository engineRepository;

    @Override
    public Optional<AbstractCarEngine> get(String id) {
        return engineRepository.findById(id);
    }

    @Override
    public List<AbstractCarEngine> getAll() {
        return engineRepository.findAll();
    }

    @Override
    public List<AbstractCarEngine> get(List<String> ids) {
        return engineRepository.findAllById(ids);
    }

    @Override
    public void save(AbstractCarEngine obj) {
        engineRepository.save(obj);
    }
}
