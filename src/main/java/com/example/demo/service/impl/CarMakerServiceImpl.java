package com.example.demo.service.impl;

import com.example.demo.model.CarMaker;
import com.example.demo.repository.CarMakerRepository;
import com.example.demo.service.CarMakerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarMakerServiceImpl implements CarMakerService {
    private final CarMakerRepository makerRepository;

    @Override
    public Optional<CarMaker> get(String id) {
        return makerRepository.findById(id);
    }

    @Override
    public List<CarMaker> getAll() {
        return makerRepository.findAll();
    }

    @Override
    public List<CarMaker> get(List<String> ids) {
        return makerRepository.findAllById(ids);
    }

    @Override
    public void save(CarMaker obj) {
        makerRepository.saveAndFlush(obj);
    }
}
