package com.example.demo.service.impl;

import com.example.demo.model.Car;
import com.example.demo.repository.CarRepository;
import com.example.demo.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {
    private final CarRepository carRepository;

    @Override
    public Optional<Car> get(String id) {
        return carRepository.findById(id);
    }

    @Override
    public List<Car> getAll() {
        return carRepository.findAll();
    }

    @Override
    public List<Car> get(List<String> ids) {
        return carRepository.findAllById(ids);
    }

    @Override
    public void save(Car obj) {
        carRepository.save(obj);
    }
}
