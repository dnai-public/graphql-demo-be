package com.example.demo.service.impl;

import com.example.demo.model.CarModel;
import com.example.demo.repository.ModelRepository;
import com.example.demo.service.CarModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarModelServiceImpl implements CarModelService {
    private final ModelRepository modelRepository;

    @Override
    public Optional<CarModel> get(String id) {
        return modelRepository.findById(id);
    }

    @Override
    public List<CarModel> getAll() {
        return modelRepository.findAll();
    }

    @Override
    public List<CarModel> get(List<String> ids) {
        return modelRepository.findAllById(ids);
    }

    @Override
    public void save(CarModel obj) {
        modelRepository.saveAndFlush(obj);
    }
}
