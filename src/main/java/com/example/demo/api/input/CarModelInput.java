package com.example.demo.api.input;

import java.util.List;

public record CarModelInput(
        String modelNumber,
        String name,
        String body,
        String manufacturerId,
        List<String> engineCodes
) {}
