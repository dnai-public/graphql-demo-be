package com.example.demo.api.input;

public record CarInput(
        String vin,
        String license,
        String engineCode,
        String modelCode,
        int mileage,
        int year
) {}
