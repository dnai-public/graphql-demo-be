package com.example.demo.api.input;

public record CarMakerInput(
        String name,
        String country,
        int established
){}
