package com.example.demo.api.input.filters;

import java.util.List;

public record EngineFilter(
        Integer powerMin,
        Integer powerMax,
        List<String> models,
        Integer torqueMin,
        Integer torqueMax
) {}
