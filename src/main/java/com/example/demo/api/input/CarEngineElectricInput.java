package com.example.demo.api.input;

import com.example.demo.model.enums.EElectricEngineType;

import java.util.List;

public record CarEngineElectricInput(
        String code,
        int power,
        int torque,
        int voltage,
        EElectricEngineType engineType,
        List<String> modelIds
) {}
