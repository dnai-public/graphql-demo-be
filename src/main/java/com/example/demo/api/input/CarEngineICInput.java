package com.example.demo.api.input;

import com.example.demo.model.enums.EFuelType;
import java.util.List;

public record CarEngineICInput(
        String code,
        int power,
        int torque,
        int cylinderCount,
        int displacement,
        EFuelType fuelType,
        List<String> modelIds
) {}
