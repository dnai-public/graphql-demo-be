package com.example.demo.api.rest;

import com.example.demo.api.input.CarInput;
import com.example.demo.mappers.CarMapper;
import com.example.demo.model.Car;
import com.example.demo.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CarRestController {
    private final CarService carService;
    private final CarMapper carMapper;

    @GetMapping("/cars/{name}")
    public ResponseEntity<Car> getCarModel(@PathVariable("name") String name) {
        return carService.get(name)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/cars")
    public ResponseEntity<List<Car>> getCarModel() {
        return ResponseEntity.ok(carService.getAll());
    }

    @PostMapping("/cars")
    public ResponseEntity<Car> postCarModel(RequestEntity<CarInput> request) {
        CarInput input = request.getBody();
        if(input == null) return ResponseEntity.badRequest().build();

        Car car = carMapper.fromInput(input);
        carService.save(car);
        return ResponseEntity.ok(car);
    }
}
