package com.example.demo.api.rest;

import com.example.demo.mappers.CarMakerMapper;
import com.example.demo.model.CarMaker;
import com.example.demo.api.input.CarMakerInput;
import com.example.demo.service.CarMakerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CarMakerRestController {
    private final CarMakerService carMakerService;
    private final CarMakerMapper carMakerMapper;

    @GetMapping("/carmakers/{name}")
    public ResponseEntity<CarMaker> getCarMaker(@PathVariable("name") String name) {
        return carMakerService.get(name)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/carmakers")
    public ResponseEntity<List<CarMaker>> getCarMaker() {
        return ResponseEntity.ok(carMakerService.getAll());
    }

    @PostMapping("/carmakers")
    public ResponseEntity<CarMaker> postCarMaker(RequestEntity<CarMakerInput> request) {
        CarMakerInput input = request.getBody();
        if(input == null) return ResponseEntity.badRequest().build();

        CarMaker maker = carMakerMapper.fromInput(input);
        carMakerService.save(maker);
        return ResponseEntity.ok(maker);
    }
}
