package com.example.demo.api.rest;

import com.example.demo.api.input.CarModelInput;
import com.example.demo.mappers.CarModelMapper;
import com.example.demo.model.CarModel;
import com.example.demo.service.CarModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CarModelRestController {
    private final CarModelService modelService;
    private final CarModelMapper modelMapper;

    @GetMapping("/models/{name}")
    public ResponseEntity<CarModel> getCarModel(@PathVariable("name") String name) {
        return modelService.get(name)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/models")
    public ResponseEntity<List<CarModel>> getCarModel() {
        return ResponseEntity.ok(modelService.getAll());
    }

    @PostMapping("/models")
    public ResponseEntity<CarModel> postCarModel(RequestEntity<CarModelInput> request) {
        CarModelInput input = request.getBody();
        if(input == null) return ResponseEntity.badRequest().build();

        CarModel model = modelMapper.fromInput(input);
        modelService.save(model);
        return ResponseEntity.ok(model);
    }
}
