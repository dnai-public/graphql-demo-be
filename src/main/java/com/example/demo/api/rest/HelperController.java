package com.example.demo.api.rest;

import com.example.demo.api.input.*;
import com.example.demo.mappers.*;
import com.example.demo.model.enums.EElectricEngineType;
import com.example.demo.model.enums.EFuelType;
import com.example.demo.service.CarEngineService;
import com.example.demo.service.CarMakerService;
import com.example.demo.service.CarModelService;
import com.example.demo.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class HelperController {
    static private boolean dataInitDone = false;
    private final CarMakerService makerService;
    private final CarMakerMapper makerMapper;

    private final CarEngineService engineService;
    private final CarEngineICMapper engineICMapper;
    private final CarEngineElectricMapper engineElectricMapper;

    private final CarModelService modelService;
    private final CarModelMapper modelMapper;

    private final CarService carService;
    private final CarMapper carMapper;

    @PostMapping("/init")
    public ResponseEntity<String> initData(){
        if(dataInitDone) return ResponseEntity.ok("Already done");

        getCarMakerInputs().stream()
                .map(makerMapper::fromInput)
                .forEach(makerService::save);

        getEngineICInputs().stream()
                .map(engineICMapper::fromInput)
                .forEach(engineService::save);

        getEngineElectricInputs().stream()
                .map(engineElectricMapper::fromInput)
                .forEach(engineService::save);

        getModelInputs().stream()
                .map(modelMapper::fromInput)
                .forEach(modelService::save);

        getCarInputs().stream()
                .map(carMapper::fromInput)
                .forEach(carService::save);

        dataInitDone = true;

        return ResponseEntity.ok("Initialized");
    }

    private List<CarMakerInput> getCarMakerInputs(){
        return List.of(
                new CarMakerInput(
                        "Audi",
                        "DE",
                        1900
                ),
                new CarMakerInput(
                        "Volvo",
                        "SWE",
                        1924
                ),
                new CarMakerInput(
                        "Skoda",
                        "CZ",
                        1888
                ),
                new CarMakerInput(
                        "GMC",
                        "USA",
                        1930
                )
        );
    }

    List<CarEngineICInput> getEngineICInputs(){
        return List.of(
                new CarEngineICInput(
                        "B6304T2",
                        242,
                        500,
                        6,
                        3000,
                        EFuelType.GASOLINE,
                        List.of()
                ),
                new CarEngineICInput(
                        "CFFB",
                        103,
                        330,
                        4,
                        1996,
                        EFuelType.DIESEL,
                        List.of()
                ),
                new CarEngineICInput(
                        "2JZ",
                        180,
                        420,
                        6,
                        3000,
                        EFuelType.GASOLINE,
                        List.of()
                ),
                new CarEngineICInput(
                        "HTP",
                        55,
                        120,
                        3,
                        1200,
                        EFuelType.GASOLINE,
                        List.of()
                ),
                new CarEngineICInput(
                        "BVN",
                        330,
                        800,
                        8,
                        4200,
                        EFuelType.DIESEL,
                        List.of()
                ),
                new CarEngineICInput(
                        "OM606",
                        120,
                        250,
                        6,
                        3000,
                        EFuelType.DIESEL,
                        List.of()
                )
        );
    }

    private List<CarEngineElectricInput> getEngineElectricInputs(){
        return List.of(
                new CarEngineElectricInput(
                        "ELE-1",
                        125,
                        600,
                        215,
                        EElectricEngineType.AC,
                        List.of()
                ),
                new CarEngineElectricInput(
                        "ELE-2",
                        200,
                        700,
                        350,
                        EElectricEngineType.PMSM,
                        List.of()
                ),
                new CarEngineElectricInput(
                        "ELE-3",
                        350,
                        1200,
                        421,
                        EElectricEngineType.BLDC,
                        List.of()
                )
        );
    }

    private List<CarModelInput> getModelInputs(){
        return List.of(
                new CarModelInput(
                        "GMC1",
                        "Model T",
                        "Hatchback",
                        "GMC",
                        List.of("HTP","ELE-1")
                ),
                new CarModelInput(
                        "A1",
                        "Audi A1",
                        "Hatchback",
                        "Audi",
                        List.of("HTP","ELE-1")
                ),
                new CarModelInput(
                        "AE2",
                        "Audi A2 e-tron",
                        "Hatchback",
                        "Audi",
                        List.of("ELE-2","ELE-1")
                ),
                new CarModelInput(
                        "V1",
                        "V70",
                        "Kombi",
                        "Volvo",
                        List.of("B6304T2")
                ),
                new CarModelInput(
                        "V2",
                        "S80",
                        "Sedan",
                        "Volvo",
                        List.of("ELE-2", "B6304T2", "BVN")
                ),
                new CarModelInput(
                        "S1",
                        "Fobie",
                        "Hatchback",
                        "Skoda",
                        List.of("HTP")
                )
        );
    }

    private List<CarInput> getCarInputs(){
        return List.of(
                new CarInput(
                        "CAR1",
                        "CAR001",
                        "HTP",
                        "A1",
                        12000,
                        2015
                ),
                new CarInput(
                        "CAR2",
                        "CAR002",
                        "ELE-1",
                        "A1",
                        15000,
                        2014
                ),
                new CarInput(
                        "CAR3",
                        "CAR003",
                        "B6304T2",
                        "V1",
                        430000,
                        2008
                ),
                new CarInput(
                        "CAR4",
                        "CAR004",
                        "HTP",
                        "S1",
                        96000,
                        2003
                ),
                new CarInput(
                        "CAR4",
                        "CAR004",
                        "ELE-1",
                        "GMC1",
                        86200,
                        2013
                )
        );
    }

}
