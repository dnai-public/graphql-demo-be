package com.example.demo.api.rest;

import com.example.demo.mappers.CarEngineElectricMapper;
import com.example.demo.mappers.CarEngineICMapper;
import com.example.demo.model.AbstractCarEngine;
import com.example.demo.model.CarEngineElectric;
import com.example.demo.model.CarEngineIC;
import com.example.demo.api.input.CarEngineElectricInput;
import com.example.demo.api.input.CarEngineICInput;
import com.example.demo.service.CarEngineService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CarEngineRestController {
    private final CarEngineService engineService;
    private final CarEngineElectricMapper engineElectricMapper;
    private final CarEngineICMapper engineICMapper;

    @GetMapping("/engines/{code}")
    public ResponseEntity<AbstractCarEngine> getCarEngine(@PathVariable("code") String code) {
        return engineService.get(code)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/engines")
    public ResponseEntity<List<AbstractCarEngine>> getCarEngine() {
        return ResponseEntity.ok(engineService.getAll());
    }

    @PostMapping("/engines-electric")
    public ResponseEntity<CarEngineElectric> postCarEngineElectric(RequestEntity<CarEngineElectricInput> request) {
        CarEngineElectricInput input = request.getBody();
        if(input == null) return ResponseEntity.badRequest().build();

        CarEngineElectric engine = engineElectricMapper.fromInput(input);
        engineService.save(engine);
        return ResponseEntity.ok(engine);
    }

    @PostMapping("/engines-ic")
    public ResponseEntity<CarEngineIC> postCarEngineIC(RequestEntity<CarEngineICInput> request) {
        CarEngineICInput input = request.getBody();
        if(input == null) return ResponseEntity.badRequest().build();

        CarEngineIC engine = engineICMapper.fromInput(input);
        engineService.save(engine);
        return ResponseEntity.ok(engine);
    }
}
