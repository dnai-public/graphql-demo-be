package com.example.demo.api.graphql.controllers;

import com.example.demo.api.graphql.types.ElectricEngineType;
import com.example.demo.api.input.filters.EngineFilter;
import com.example.demo.model.AbstractCarEngine;
import com.example.demo.model.CarEngineElectric;
import com.example.demo.model.CarModel;
import com.example.demo.service.CarEngineService;
import com.example.demo.service.CarModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class CarModelGqlController {
    private final CarModelService modelService;
    private final CarEngineService engineService;

    @QueryMapping
    public List<CarModel> getCarModels(@Argument List<String> modelNums){
        if(modelNums.size() < 1) return modelService.getAll();
        return modelService.get(modelNums);
    }

    @QueryMapping
    public CarModel getCarModel(@Argument String modelNumber){
        return modelService.get(modelNumber).orElse(null);
    }

    @SchemaMapping(typeName = "CarModel", field = "availableEngines")
    public List<AbstractCarEngine> getAvailableEngines(CarModel model, @Argument EngineFilter filter){
        return engineService.filter(model.getAvailableEngines(), filter);
    }
}
