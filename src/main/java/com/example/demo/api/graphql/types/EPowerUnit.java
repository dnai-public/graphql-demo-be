package com.example.demo.api.graphql.types;

public enum EPowerUnit {
    KW, HP
}
