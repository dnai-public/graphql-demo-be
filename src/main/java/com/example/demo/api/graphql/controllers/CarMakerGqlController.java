package com.example.demo.api.graphql.controllers;

import com.example.demo.mappers.CarMakerMapper;
import com.example.demo.model.CarMaker;
import com.example.demo.service.CarMakerService;
import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class CarMakerGqlController {
    private final CarMakerService carMakerService;
    private final CarMakerMapper carMakerMapper;

    @QueryMapping
    public List<CarMaker> getCarMakers() {
        return carMakerService.getAll();
    }

    @QueryMapping
    public CarMaker getCarMaker(@Argument String name) {
        return carMakerService.get(name).orElse(null);
    }
}
