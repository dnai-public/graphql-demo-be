package com.example.demo.api.graphql.handlers;

import graphql.GraphQLError;
import graphql.GraphqlErrorException;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.graphql.execution.DataFetcherExceptionResolverAdapter;
import org.springframework.graphql.execution.ErrorType;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.NoSuchElementException;

@Component
public class GQLExceptionHandler extends DataFetcherExceptionResolverAdapter {

    @Override
    protected List<GraphQLError> resolveToMultipleErrors(Throwable ex, DataFetchingEnvironment env) {
        return super.resolveToMultipleErrors(ex, env);
    }

    @Override
    protected GraphQLError resolveToSingleError(Throwable ex, DataFetchingEnvironment env) {
        if(ex instanceof NoSuchElementException) {
            return GraphqlErrorException.newErrorException()
                    .cause(ex)
                    //.path(Stream.of(ex.getStackTrace()).map(StackTraceElement::toString).collect(Collectors.toList()))
                    .path(
                            List.of(
                                    env.getMergedField().getSingleField().getAlias()
                            )
                    )
                    .extensions(env.getArguments())
                    .errorClassification(ErrorType.NOT_FOUND)
                    .message(ex.getMessage())
                    .build();
        }
        if(ex instanceof IllegalArgumentException){
            return GraphqlErrorException.newErrorException()
                    .cause(ex)
                    //.path(Stream.of(ex.getStackTrace()).map(StackTraceElement::toString).collect(Collectors.toList()))
                    .path(
                            List.of(
                                    env.getMergedField().getSingleField().getAlias()
                            )
                    )
                    .extensions(env.getArguments())
                    .errorClassification(ErrorType.BAD_REQUEST)
                    .message(ex.getMessage())
                    .build();
        }
        return super.resolveToSingleError(ex, env);
    }
}
