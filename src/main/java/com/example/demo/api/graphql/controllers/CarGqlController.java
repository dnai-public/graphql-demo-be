package com.example.demo.api.graphql.controllers;

import com.example.demo.api.input.CarInput;
import com.example.demo.mappers.CarMapper;
import com.example.demo.model.Car;
import com.example.demo.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class CarGqlController {
    private final CarService carService;
    private final CarMapper carMapper;

    @QueryMapping
    public List<Car> getCars(){
        return carService.getAll();
    }

    @QueryMapping
    public Car getCarByVin(@Argument String vin){
        return carService.get(vin).orElse(null);
    }

    @QueryMapping
    public Car getCarByLic(@Argument String license){
        return carService.getAll().stream()
                .filter(t -> license.equals(t.getLicense()))
                .findFirst()
                .orElse(null);
    }

    @QueryMapping
    public Car getCar(@Argument String vin, @Argument String license){
        if(vin.length() > 0) return getCarByVin(vin);
        if(license.length() > 0) return getCarByLic(license);
        throw new IllegalArgumentException("Vin and License are both empty!");
    }

    @MutationMapping
    public Car postCar(@Argument CarInput input){
        Car car = carMapper.fromInput(input);
        carService.save(car);
        return car;
    }
}
