package com.example.demo.api.graphql.controllers;

import com.example.demo.api.graphql.types.EPowerUnit;
import com.example.demo.api.graphql.types.ElectricEngineType;
import com.example.demo.api.input.CarEngineElectricInput;
import com.example.demo.api.input.CarEngineICInput;
import com.example.demo.api.input.filters.EngineFilter;
import com.example.demo.mappers.CarEngineElectricMapper;
import com.example.demo.mappers.CarEngineICMapper;
import com.example.demo.model.AbstractCarEngine;
import com.example.demo.model.CarEngineElectric;
import com.example.demo.model.CarEngineIC;
import com.example.demo.service.CarEngineService;
import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class CarEngineGqlController {
    private final CarEngineService engineService;
    private final CarEngineElectricMapper engineElectricMapper;
    private final CarEngineICMapper engineICMapper;

    @QueryMapping
    public List<AbstractCarEngine> getCarEngines(@Argument EngineFilter filter){
        return engineService.filter(engineService.getAll(), filter);
    }

    @QueryMapping
    public AbstractCarEngine getCarEngine(@Argument String code){
        return engineService.get(code).orElse(null);
    }

    @SchemaMapping(typeName = "CarEngineElectric", field = "engineType")
    public ElectricEngineType getEngineType(CarEngineElectric engineElectric){
        return new ElectricEngineType(
                engineElectric.getEngineType().toString(),
                engineElectric.getEngineType().name
        );
    }

    @MutationMapping
    public CarEngineElectric postCarEngineElectric(@Argument CarEngineElectricInput input){
        CarEngineElectric engine = engineElectricMapper.fromInput(input);
        engineService.save(engine);
        return engine;
    }

    @MutationMapping
    public CarEngineIC postCarEngineIC(@Argument CarEngineICInput input){
        CarEngineIC engine = engineICMapper.fromInput(input);
        engineService.save(engine);
        return engine;
    }

    private Integer powerInUnit(AbstractCarEngine engine, EPowerUnit unit){
        if(unit.equals(EPowerUnit.HP)) return (int) Math.ceil(engine.getPower()*1.341);
        return engine.getPower();
    }

    @SchemaMapping(typeName = "CarEngineElectric", field = "power")
    public Integer getPower(CarEngineElectric engine, @Argument EPowerUnit unit){
        return powerInUnit(engine, unit);
    }

    @SchemaMapping(typeName = "CarEngineIC", field = "power")
    public Integer getPower(CarEngineIC engine, @Argument EPowerUnit unit){
        return powerInUnit(engine, unit);
    }
}
