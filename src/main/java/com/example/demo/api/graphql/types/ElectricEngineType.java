package com.example.demo.api.graphql.types;

public record ElectricEngineType(
        String code,
        String name
) {}
