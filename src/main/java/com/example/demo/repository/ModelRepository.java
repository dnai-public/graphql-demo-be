package com.example.demo.repository;

import com.example.demo.model.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModelRepository extends JpaRepository<CarModel, String> {
}
