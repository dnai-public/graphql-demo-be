package com.example.demo.repository;

import com.example.demo.model.AbstractCarEngine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EngineRepository extends JpaRepository<AbstractCarEngine, String> {
}
