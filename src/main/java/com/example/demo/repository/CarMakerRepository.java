package com.example.demo.repository;

import com.example.demo.model.CarMaker;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarMakerRepository extends JpaRepository<CarMaker, String> {
}
