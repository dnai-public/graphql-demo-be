package com.example.demo.mappers;

import com.example.demo.api.input.CarEngineICInput;
import com.example.demo.model.CarEngineIC;
import com.example.demo.service.CarModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class CarEngineICMapper implements BaseMapper<CarEngineIC, CarEngineICInput>{
    private final CarModelService modelService;

    @Override
    public CarEngineIC fromInput(CarEngineICInput input) {
        return CarEngineIC.builder()
                .engineCode(input.code())
                .power(input.power())
                .torque(input.torque())
                .cylinderCount(input.cylinderCount())
                .displacement(input.displacement())
                .fuelType(input.fuelType())
                .models(
                        modelService.get(
                                Stream.ofNullable(input.modelIds())
                                        .flatMap(Collection::stream)
                                        .collect(Collectors.toList())
                        )
                )
                .build();
    }
}
