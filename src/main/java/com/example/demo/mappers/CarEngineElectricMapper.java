package com.example.demo.mappers;

import com.example.demo.api.input.CarEngineElectricInput;
import com.example.demo.model.CarEngineElectric;
import com.example.demo.service.CarModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class CarEngineElectricMapper implements BaseMapper<CarEngineElectric, CarEngineElectricInput>{
    private final CarModelService modelService;

    @Override
    public CarEngineElectric fromInput(CarEngineElectricInput input) {
        return CarEngineElectric.builder()
                .engineCode(input.code())
                .power(input.power())
                .torque(input.torque())
                .voltage(input.voltage())
                .engineType(input.engineType())
                .models(
                        modelService.get(
                                Stream.ofNullable(input.modelIds())
                                        .flatMap(Collection::stream)
                                        .collect(Collectors.toList())
                        )
                )
                .build();
    }
}
