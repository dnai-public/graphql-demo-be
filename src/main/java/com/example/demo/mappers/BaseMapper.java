package com.example.demo.mappers;

public interface BaseMapper<E, I> {
    E fromInput(I input);
}
