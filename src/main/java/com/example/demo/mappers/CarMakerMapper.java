package com.example.demo.mappers;

import com.example.demo.api.input.CarMakerInput;
import com.example.demo.model.CarMaker;
import org.springframework.stereotype.Component;

@Component
public class CarMakerMapper implements BaseMapper<CarMaker, CarMakerInput>{

    @Override
    public CarMaker fromInput(CarMakerInput input) {
        return CarMaker.builder()
                .name(input.name())
                .country(input.country())
                .established(input.established())
                .build();
    }
}
