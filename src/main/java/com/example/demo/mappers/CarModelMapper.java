package com.example.demo.mappers;

import com.example.demo.api.input.CarModelInput;
import com.example.demo.model.CarModel;
import com.example.demo.service.CarEngineService;
import com.example.demo.service.CarMakerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class CarModelMapper implements BaseMapper<CarModel, CarModelInput>{
    private final CarMakerService makerService;
    private final CarEngineService engineService;

    @Override
    public CarModel fromInput(CarModelInput input) {
        return CarModel.builder()
                .modelNumber(input.modelNumber())
                .name(input.name())
                .body(input.body())
                .manufacturer(makerService.get(input.manufacturerId())
                        .orElseThrow(() -> new NoSuchElementException(
                                String.format("CarMaker with id %s not found!", input.manufacturerId())
                        ))
                )
                .availableEngines(
                        engineService.get(
                                Stream.ofNullable(input.engineCodes())
                                        .flatMap(Collection::stream)
                                        .collect(Collectors.toList())
                        )
                )
                .build();
    }
}
