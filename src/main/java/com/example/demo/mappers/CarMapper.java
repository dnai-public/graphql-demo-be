package com.example.demo.mappers;

import com.example.demo.api.input.CarInput;
import com.example.demo.model.Car;
import com.example.demo.service.CarEngineService;
import com.example.demo.service.CarModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.NoSuchElementException;

@Component
@RequiredArgsConstructor
public class CarMapper implements BaseMapper<Car, CarInput>{
    private final CarEngineService engineService;
    private final CarModelService modelService;

    @Override
    public Car fromInput(CarInput input) {
        return Car.builder()
                .vin(input.vin())
                .license(input.license())
                .mileage(input.mileage())
                .manufactureYear(input.year())
                .engine(
                        engineService.get(input.engineCode())
                                .orElseThrow(() -> new NoSuchElementException(
                                        String.format("Engine with code %s not found!", input.engineCode())
                                ))
                )
                .model(
                        modelService.get(input.modelCode())
                                .orElseThrow(() -> new NoSuchElementException(
                                        String.format("CarModel with code %s not found!", input.modelCode())
                                )))
                .build();
    }
}
