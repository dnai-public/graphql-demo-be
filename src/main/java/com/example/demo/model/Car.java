package com.example.demo.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class Car {
    @Id
    private String vin;

    @Column(unique = true, nullable = false)
    private String license;

    @ManyToOne
    private AbstractCarEngine engine;

    @ManyToOne
    private CarModel model;
    private int mileage;
    private int manufactureYear;

}
