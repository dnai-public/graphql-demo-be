package com.example.demo.model;

import com.example.demo.model.enums.EElectricEngineType;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Getter
@Setter
@Entity
public class CarEngineElectric extends AbstractCarEngine{
    protected int voltage;
    protected EElectricEngineType engineType;
}
