package com.example.demo.model;

import com.example.demo.model.enums.EFuelType;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Getter
@Setter
@Entity
public class CarEngineIC extends AbstractCarEngine{
    protected int displacement;
    protected int cylinderCount;
    protected EFuelType fuelType;
}
