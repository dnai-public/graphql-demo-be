package com.example.demo.model.enums;

public enum EElectricEngineType {
    DC("Direct Current motor"),
    BLDC("Brushless DC motor"),
    PMSM("Permanent Magnet Synchronous Motor"),
    AC("Three phase AC motor");

    public final String name;

    EElectricEngineType(String name) {
        this.name = name;
    }
}
