package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class CarModel {
    @Id
    private String modelNumber;
    private String name;
    private String body;

    @ManyToOne
    @JsonBackReference
    private CarMaker manufacturer;

    @ManyToMany(
            fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE}
    )
    @JoinTable(
            name = "model_engine",
            joinColumns = { @JoinColumn(name = "modelNumber") },
            inverseJoinColumns = { @JoinColumn(name = "engineCode") }
    )
    @JsonManagedReference
    private List<AbstractCarEngine> availableEngines = new ArrayList<>();
}
