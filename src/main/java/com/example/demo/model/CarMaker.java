package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class CarMaker{
    @Id
    private String name;
    private String country;
    private int established;

    @OneToMany(mappedBy = "manufacturer")
    @JsonManagedReference
    private List<CarModel> models = new ArrayList<>();
}
